package com.example.finalapp.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class NoteEntity(
    @PrimaryKey(autoGenerate = true)
    val noteId : Long = 0,
    var text : String,
)
