package com.example.finalapp.database.dao

import androidx.room.*
import com.example.finalapp.database.entities.ImageWithNote
import com.example.finalapp.database.entities.NoteEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(noteEntity: NoteEntity)

    @Query("SELECT * FROM NoteEntity")
    fun selectAll(): Flow<List<NoteEntity>>

    @Update
    fun updateNote(note: NoteEntity)

    @Query("Select * From NoteEntity where noteId=:id")
    fun findById(id : String) : NoteEntity

    @Query("DELETE FROM NoteEntity")
    fun deleteAll()

    @Query("DELETE From NoteEntity where noteId=:id")
    fun deleteById(id : String)
    @Transaction
    @Query("SELECT * FROM NoteEntity where noteId=:id")
    fun findNotesWithImagesById(id: String): ImageWithNote

}