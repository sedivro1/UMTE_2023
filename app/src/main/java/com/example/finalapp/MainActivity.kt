package com.example.finalapp

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.finalapp.ui.Navigation
import com.example.finalapp.ui.theme.FinalAppTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FinalAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {
                    MainScreen()
                }
            }
        }
    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MainScreen() {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    val scope = rememberCoroutineScope()
    val navController = rememberNavController()
    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = { Text("UHK Notes App") },
                navigationIcon = {
                    IconButton(onClick = {
                        scope.launch {
                            scaffoldState.drawerState.open()
                        }

                    }) {
                        Icon(Icons.Filled.Menu, "")
                    }

                }
            )
        },
        drawerContent = {
            Column() {
                DrawerColumnItem("home", "Home", navController, scaffoldState, scope)
                DrawerColumnItem("notes", "Add note", navController, scaffoldState, scope)
            }
        }
    ) {
        Navigation(navController, scope)
    }
}

@Composable
fun DrawerColumnItem(
    navigationRoute: String, navigationText: String, navController: NavHostController, scaffoldState: ScaffoldState,
    scope: CoroutineScope
) {
    Button(
        onClick = {
            navController.navigate(navigationRoute)
            scope.launch {
                scaffoldState.drawerState.close()
            }
        },
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(navigationText)
    }
}

