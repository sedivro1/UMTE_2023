package com.example.finalapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.finalapp.database.dao.ImageDao
import com.example.finalapp.database.dao.NoteDao
import com.example.finalapp.database.entities.ImageEntity
import com.example.finalapp.database.entities.NoteEntity

@Database(
    entities = [NoteEntity::class, ImageEntity::class],
    version = AppDatabase.Version
)
abstract class AppDatabase: RoomDatabase() {

    abstract fun noteDao() : NoteDao
    abstract fun imageDao() : ImageDao

    companion object {
        const val Version = 4
        const val Name = "NoteProjectDb"
    }
}