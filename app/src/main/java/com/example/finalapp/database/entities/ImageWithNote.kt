package com.example.finalapp.database.entities

import androidx.room.Embedded
import androidx.room.Relation

data class ImageWithNote(
    @Embedded
    val note: NoteEntity,
    @Relation(
        parentColumn = "noteId",
        entityColumn = "noteId"
    )
    val imageList: List<ImageEntity>?

)
