package com.example.finalapp.di

import androidx.room.Room
import com.example.finalapp.database.AppDatabase
import com.example.finalapp.ui.db.DatabaseViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(
            context = androidApplication(),
            klass = AppDatabase::class.java,
            name = AppDatabase.Name
        ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
    }

    single { get<AppDatabase>().noteDao() }
    single { get<AppDatabase>().imageDao() }
}

val uiModule = module {
    viewModel { DatabaseViewModel(get(), get()) }
}

