package com.example.finalapp.ui.note

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.finalapp.ui.db.DatabaseViewModel
import kotlinx.coroutines.CoroutineScope
import org.koin.androidx.compose.getViewModel

@Composable
fun AddNote(scope: CoroutineScope, viewModel: DatabaseViewModel = getViewModel()) {


    var noteHeader by remember { mutableStateOf("") }
    var text by remember { mutableStateOf("") }


    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        TextField(
            value = text,
            onValueChange = { text = it },
            label = { Text("Note text") },
            modifier = Modifier.fillMaxWidth().fillMaxHeight(0.9f)
        )

        Button(onClick = {
            viewModel.createNote(text)
            text = ""
        }, modifier = Modifier.fillMaxWidth()) {
            Text("Save")
        }

    }
}