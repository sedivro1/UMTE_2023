package com.example.finalapp.database.dao

import androidx.room.*
import com.example.finalapp.database.entities.ImageEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ImageDao {
    @Insert
    fun insertOrUpdate(imageEntity: ImageEntity)

    @Query("SELECT * FROM ImageEntity")
    fun selectAll(): Flow<List<ImageEntity>>

    @Query("Select * From ImageEntity where imageId=:id")
    fun findById(id : String) : ImageEntity

    @Query("DELETE FROM ImageEntity")
    fun deleteAll()

    @Query("DELETE From ImageEntity where imageId=:id")
    fun deleteById(id : String)
}