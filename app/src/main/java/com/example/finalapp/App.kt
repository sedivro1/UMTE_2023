package com.example.finalapp

import android.app.Application
import com.example.finalapp.di.databaseModule
import com.example.finalapp.di.uiModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(applicationContext)
            modules(listOf(databaseModule, uiModule))
        }
    }
}