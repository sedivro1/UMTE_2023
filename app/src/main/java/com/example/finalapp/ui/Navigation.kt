package com.example.finalapp.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.finalapp.ui.note.AddNote
import com.example.finalapp.ui.note.ImageGallery
import com.example.finalapp.ui.note.NoteDetail
import com.example.finalapp.ui.note.NotesList
import kotlinx.coroutines.CoroutineScope

@Composable
fun Navigation(navController: NavHostController, scope: CoroutineScope) {
    NavHost(navController, startDestination = "home") {

        composable("home") {
            NotesList(navController)
        }

        composable("notes") {
            AddNote(scope)
        }
        composable(
            "notes/{noteId}",
            arguments = listOf(navArgument("noteId") { type = NavType.StringType })
        ) { backStackEntry ->
            backStackEntry.arguments!!.getString("noteId")?.let { NoteDetail(it, navController) }
        }
        composable(
            "images/{imageId}",
            arguments = listOf(navArgument("imageId") { type = NavType.StringType })
        ) { backStackEntry ->
            backStackEntry.arguments!!.getString("imageId")?.let { ImageGallery(it) }
        }
    }
}