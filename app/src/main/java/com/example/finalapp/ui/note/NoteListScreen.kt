package com.example.finalapp.ui.note

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.finalapp.ui.db.DatabaseViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun NotesList(navController: NavHostController, viewModel: DatabaseViewModel = getViewModel()) {
    val notes = viewModel.notes.collectAsState(emptyList())
    var textInNoteSearch by remember { mutableStateOf("") }

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TextField(
            value = textInNoteSearch,
            onValueChange = { textInNoteSearch = it },
            label = { Text("Fulltext search") },
            modifier = Modifier.padding(16.dp)
        )
        Column (
            modifier = Modifier.verticalScroll(rememberScrollState())
        ){
            notes.value.filter { value -> textInNoteSearch.isEmpty() || value.text.contains(textInNoteSearch)  }
                .map { value ->
                    Note(
                        text = value.text
                    ) { navController.navigate("notes/" + value.noteId) }
                }
        }

    }
}

@Composable
fun Note(
    text: String,
    onClick: () -> Unit
) {
    val noteStringLength = 40
    val textToShow : String = if(text.length > noteStringLength){
        text.substring(noteStringLength)
    }else{
        text
    }

    Box(Modifier.fillMaxWidth().background(color = Color.Gray).clickable(onClick = onClick)) {
        Text(
            text = textToShow,
            modifier = Modifier.padding(16.dp)
        )
    }
}

