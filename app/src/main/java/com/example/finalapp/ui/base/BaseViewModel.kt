package com.example.finalapp.ui.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow

abstract class BaseViewModel : ViewModel() {

    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Default + job)

    private val _state = MutableStateFlow<State>(State.None)

    protected fun <Result> launch(
        onError: ((Throwable) -> Unit)? = null,
        state: MutableStateFlow<State>? = _state,
        block: (suspend CoroutineScope.() -> Result)
    ) = scope.launch(throwableHandler<Result>(onError, state)) {


        state?.emit(State.Loading)

        val result = block()

        state?.emit(State.Success(result))
    }

    private fun <Result> throwableHandler(
        onError: ((Throwable) -> Unit)? = null,
        state: MutableStateFlow<State>?,
    ) = CoroutineExceptionHandler { _, throwable ->
        onError?.invoke(throwable)
        state?.tryEmit(
            State.Failure(throwable)
        )
    }

}
