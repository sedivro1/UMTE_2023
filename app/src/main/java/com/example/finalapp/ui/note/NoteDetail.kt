package com.example.finalapp.ui.note

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.MediaStore
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.finalapp.ui.db.DatabaseViewModel
import org.koin.androidx.compose.getViewModel
import java.io.ByteArrayOutputStream


@Composable
fun NoteDetail(noteId: String, navController: NavHostController, viewModel: DatabaseViewModel = getViewModel()) {
    val noteEntity = viewModel.findNoteById(noteId)
    var text by remember { mutableStateOf(noteEntity.text) }
    var state by remember { mutableStateOf("basic") }
    val context = LocalContext.current
    val bitmap = remember {
        mutableStateOf<Bitmap?>(null)
    }
    val galleryLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            if (uri != null) {
                val stream = ByteArrayOutputStream()
                val btm1 = MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
                btm1!!.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val bitmapdata: ByteArray = stream.toByteArray()
                viewModel.createImageForNote(noteId = noteEntity.noteId, imageBytes = bitmapdata)
            }
        }


    bitmap.value?.let { btm ->
        Image(
            bitmap = btm.asImageBitmap(),
            contentDescription = null,
            modifier = Modifier.size(400.dp)
        )
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        if (state == "basic") {
            Text(
                text = text,
                modifier = Modifier.fillMaxWidth().fillMaxHeight(0.6f).offset(5.dp, 10.dp)
            )
            Button(onClick = {
                state = "edit"
            }, modifier = Modifier.fillMaxWidth()) {
                Text("Edit")
            }
            Button(onClick = {
                galleryLauncher.launch("image/*")
            }, modifier = Modifier.fillMaxWidth()) {
                Text("Add note image")
            }
            Button(onClick = {
                navController.navigate("images/" + noteEntity.noteId)
            }, modifier = Modifier.fillMaxWidth()) {
                Text("Show note image")
            }
            Button(onClick = {
                viewModel.deleteById(noteEntity.noteId.toString())
                navController.navigate("home")
            }, modifier = Modifier.fillMaxWidth()) {
                Text("Delete note")
            }
        } else {
            TextField(
                value = text,
                onValueChange = { text = it },
                label = { Text("Note text") },
                modifier = Modifier.fillMaxWidth().fillMaxHeight(0.7f)
            )
            Button(onClick = {
                noteEntity.text = text
                viewModel.updateNote(noteEntity)
                navController.navigate("home")
            }, modifier = Modifier.fillMaxWidth()) {
                Text("Edit")
            }
            Button(onClick = {
                state = "basic"
            }, modifier = Modifier.fillMaxWidth()) {
                Text("Preview")
            }
        }

    }
}

@Composable
fun ImageGallery(noteId: String, viewModel: DatabaseViewModel = getViewModel()) {
    val images = viewModel.findImagesByNoteId(noteId).imageList


    images?.let {
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState()).fillMaxWidth(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            for (image in it) {
                val btm = BitmapFactory.decodeByteArray(image.imageBytes, 0, image.imageBytes.size)
                Image(
                    bitmap = btm.asImageBitmap(),
                    contentDescription = null,
                    modifier = Modifier.size(200.dp).padding(5.dp)
                )
            }
        }

    }
}