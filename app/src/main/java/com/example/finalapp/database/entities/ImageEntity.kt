package com.example.finalapp.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [ForeignKey(
        entity = NoteEntity::class,
        parentColumns = arrayOf("noteId"),
        childColumns = arrayOf("noteId"),
        onDelete = ForeignKey.CASCADE
    )]
)
data class ImageEntity(
    @PrimaryKey(autoGenerate = true)
    val imageId : Long = 0,
    @ColumnInfo(index = true)
    val noteId: Long,
    var imageBytes : ByteArray
)
