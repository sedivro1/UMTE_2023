package com.example.finalapp.ui.db

import com.example.finalapp.database.dao.ImageDao
import com.example.finalapp.database.dao.NoteDao
import com.example.finalapp.database.entities.ImageEntity
import com.example.finalapp.database.entities.ImageWithNote
import com.example.finalapp.database.entities.NoteEntity
import com.example.finalapp.ui.base.BaseViewModel
import kotlinx.coroutines.runBlocking

class DatabaseViewModel(
    private val noteDao: NoteDao,
    private val imageDao: ImageDao
) : BaseViewModel() {

    val notes = noteDao.selectAll()

    fun createNote(text: String) {
        launch {
            noteDao.insertOrUpdate(NoteEntity(text = text))
        }
    }

    fun createImageForNote(noteId : Long, imageBytes : ByteArray){
        launch {
            imageDao.insertOrUpdate(ImageEntity(noteId = noteId, imageBytes = imageBytes))
        }
    }

    fun findNoteById(id: String): NoteEntity {
        return runBlocking  {
            noteDao.findById(id)
        }
    }

    fun findImagesByNoteId(id: String): ImageWithNote {
        return runBlocking  {
            noteDao.findNotesWithImagesById(id)
        }
    }

    fun updateNote(noteEntity: NoteEntity){
        launch {
            noteDao.updateNote(noteEntity)
        }
    }

    fun deleteById(id : String) {
        launch {
            noteDao.deleteById(id)
        }
    }

}
